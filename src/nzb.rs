use std::io::BufRead;

use quick_xml::de::from_reader;
use serde::{self, Deserialize, Deserializer, Serialize};

use crate::article::MessageId;

#[derive(Debug, Deserialize, Serialize)]
pub struct Nzb {
  #[serde(rename = "head", deserialize_with = "unwrap_metadata", default)]
  pub metadata: Vec<Meta>,
  #[serde(rename = "file")]
  pub files: Vec<File>,
}

pub enum NzbError {
  DeserializationFailed(quick_xml::DeError),
  OpenFailed(std::io::Error),
}

fn unwrap_metadata<'de, D>(deserializer: D) -> Result<Vec<Meta>, D::Error>
where
  D: Deserializer<'de>,
{
  Ok(Head::deserialize(deserializer)?.children)
}

impl Nzb {
  pub fn read_from<R: BufRead>(reader: R) -> Result<Nzb, quick_xml::DeError> {
    from_reader(reader)
  }

  pub fn size(&self) -> u64 {
    self
      .files
      .iter()
      .flat_map(|file| file.segments.iter().map(|seg| seg.size))
      .sum()
  }

  pub fn name(&self) -> Option<&str> {
    self.metadata.iter().find_map(|m| {
      if m.key == "name" {
        Some(m.value.as_str())
      } else {
        None
      }
    })
  }

  pub fn set_name(&mut self, name: &str) {
    self.metadata.push(Meta {
      key: "name".into(),
      value: name.into(),
    })
  }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Head {
  #[serde(rename = "meta", default)]
  pub children: Vec<Meta>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Meta {
  #[serde(rename = "type")]
  pub key: String,
  #[serde(rename = "$value")]
  pub value: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct File {
  pub poster: String,
  pub date: usize,
  pub subject: String,
  #[serde(deserialize_with = "unwrap_groups")]
  pub groups: Vec<String>,
  #[serde(deserialize_with = "unwrap_segments")]
  pub segments: Vec<Segment>,
}

fn unwrap_groups<'de, D>(deserializer: D) -> Result<Vec<String>, D::Error>
where
  D: Deserializer<'de>,
{
  Ok(Groups::deserialize(deserializer)?.children)
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Groups {
  #[serde(rename = "group")]
  pub children: Vec<String>,
}

fn unwrap_segments<'de, D>(deserializer: D) -> Result<Vec<Segment>, D::Error>
where
  D: Deserializer<'de>,
{
  Ok(Segments::deserialize(deserializer)?.children)
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Segments {
  #[serde(rename = "segment")]
  pub children: Vec<Segment>,
}

#[derive(Debug, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub struct Segment {
  #[serde(rename = "bytes")]
  pub size: u64,
  #[serde(rename = "number")]
  pub order: usize,
  #[serde(rename = "$value")]
  pub message_id: MessageId,
}
