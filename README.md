# rews

rews is a binary client for the Usenet network.

## Purpose
The main purpose of this application is to provide a CLI tool for users to use Usenet as a cloud drive in a similar manner to products like Dropbox, OneDrive, iCloud etc.

A later goal is to also provide integration with the Windows Cloud API.

## Roadmap

### 1.0
* [x] Secure connections over TLS
* [x] Support for shell completions
* [x] Download binary files
* [ ] Upload binary files
* [ ] Support for encryption
* [ ] Support for parity volumes

### 2.0
* [ ] Support for Windows Cloud API
